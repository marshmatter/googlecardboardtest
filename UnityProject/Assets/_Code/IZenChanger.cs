﻿using UnityEngine;
using System.Collections;

public interface IZenChanger
{
	/// <summary>
	/// Disabling mechanism for the object that changed the zen state.
	/// Ideally used to disable/reset/wind-down any "Active" state animation. 
	/// </summary>
	void Deactivate();

	/// <summary>
	/// Reset Scene to Default Zen State. 
	/// </summary>
	void ResetAll();
}
