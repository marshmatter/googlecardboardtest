﻿using UnityEngine;
using System.Collections;
/// <summary>
/// A Zen Crystal that triggers a change in the scene state and then spins until deactivated. 
/// </summary>
public class ZenCrystal : StateMonoBehaviour, IZenChanger
{
	/// <summary>
	/// Max spin rate of the crysal when active. 
	/// </summary>
	[SerializeField] float spinRate = 1.0f;

	/// <summary>
	/// The Current spin rate of the crystal. 
	/// </summary>
	float currentSpinRate = 0f; 

	bool _isActive;
	/// <summary>
	/// Is this crystal active? If setting to active, will disable collider so it can't be re-triggered. 
	/// </summary>
	bool isActive
	{
		get
		{
			return _isActive;
		}
		set
		{
			_isActive = value;
			GetComponent<Collider>().enabled = !value;
		}
	}

	/// <summary>
	/// Passes new Zen preset data to the Controller. 
	/// </summary>
	/// <param name="preset"></param>
	public void ActivateZenType(ZenType preset)
	{
		// if already active, nevermind. 
		if (isActive)
			return;

		// Zen data to the controller.
		DemoController.Instance.ChangeZenState(preset, this);

		// start the active state. 
		SetState(Active);
	}

	#region IZenChanger interface
	public void Deactivate()
	{
		SetState(Idle);
	}

	public void ResetAll()
	{
		DemoController.Instance.ResetAll(); 
	}
	#endregion

	/// <summary>
	/// Rotates the Crystal if need. Clamps to above 0. 
	/// </summary>
	void Rotate()
	{
		// Clamp to above 0f
		if (currentSpinRate > 0f)
		{
			transform.Rotate(Vector3.up, currentSpinRate * Time.deltaTime);
			transform.Rotate(Vector3.left, currentSpinRate * Time.deltaTime);
		}
		return;
	}
	
	#region unity interface
	void Start()
	{
		SetState(Idle);
	}
	#endregion

	#region states
	IEnumerator Idle()
	{
		float _elapsedTime = 0f;
		float _rampDownSpeed = 2.0f;

		// ramp down if spinning. 
		while (currentSpinRate > 0f && _elapsedTime <= _rampDownSpeed)
		{
			currentSpinRate = Mathf.Lerp(spinRate, 0f, Tween.EaseValue(Tween.EaseType.Linear, _elapsedTime));
			Rotate();
			_elapsedTime += Time.deltaTime / _rampDownSpeed;
			yield return null;
		}
		isActive = false;
		yield return null;
	}

	IEnumerator Active()
	{
		float _elapsedTime = 0f; 
		float _rampSpeed = 2.0f;
		isActive = true;

		// ramp up rotation
		while (_elapsedTime <= _rampSpeed)
		{
			currentSpinRate = Mathf.Lerp(0f, spinRate, Tween.EaseValue(Tween.EaseType.SineIn, _elapsedTime));
			Rotate();
			_elapsedTime += Time.deltaTime / _rampSpeed;
			yield return null;
		}

		// keep rotating until state change. 
		while (true)
		{
			Rotate();
			yield return null;
		}
	}
	#endregion
}
