﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Controls the scene's zen state. 
/// </summary>
public class DemoController : Singleton<DemoController>, IZenChanger {
	[SerializeField]
	Light sceneLight;

	[SerializeField]
	ZenType defaultZen;

	[SerializeField]
	float transitionTime;

	// If a new zen is triggered during a transition, it is stored here for queing. 
	ZenType queuedZen;

	/// <summary>
	/// Gets or sets the scene's zen state. Will update the scene to the ZenType attributes. 
	/// </summary>
	ZenType _currentZen;
	ZenType currentZen
	{
		get
		{
			return _currentZen;
		}
		set
		{
			// Update all the Zen properties in the scene. 
			sceneLight.intensity = value.lightIntensity;
			sceneLight.color = value.lightColor;
			UpdateCameraColour(mainCamera, value.backgroundColor);
			RenderSettings.ambientLight = value.backgroundColor;
			_currentZen = value;
		}
	}

	Camera mainCamera;
	bool transitionActive;

	/// <summary>
	/// Remember the ZenChanger that triggered the change. 
	/// </summary>
	IZenChanger activeChanger; 

	void Awake()
	{
		// Set up Google Cardboard on the main camera. 
		mainCamera = Camera.main;
		mainCamera.gameObject.AddComponent<StereoController>();
	}

	void Start()
	{
		// Set the current zen state to default. 
		currentZen = defaultZen;
	}

	void LateUpdate()
	{
		UpdateCardboard();
	}

	void UpdateCardboard()
	{
		Cardboard.SDK.UpdateState();
		if (Cardboard.SDK.BackButtonPressed)
		{
			Application.Quit();
		}
	}

	/// <summary>
	/// Updates the camera background colour for all VR cameras. 
	/// </summary>
	/// <param name="camera">Parent camera</param>
	/// <param name="color">Color to update to.</param>
	void UpdateCameraColour(Camera camera, Color color)
	{
		Camera[] _allCameras = camera.gameObject.GetComponentsInChildren<Camera>();

		try
		{
			for (int i = 0; i < _allCameras.Length; i++)
			{
				_allCameras[i].backgroundColor = color;
			}
		}
		catch
		{
			Debug.Log("No Cameras Found on Background Colour update");
		}
	
	}

	/// <summary>
	/// Initiates a change in the scene's zen state. 
	/// </summary>
	/// <param name="newZen">Zen data asset</param>
	/// <param name="source">Origin of the zen change.</param>
	public void ChangeZenState(ZenType newZen, IZenChanger source)
	{
		if (currentZen == newZen)
			return;

		if (transitionActive)
		{
			queuedZen = newZen;
		}

		StartCoroutine(ZenTransition(newZen, transitionTime, source));
	}

	/// <summary>
	/// Transitions to a new zen state
	/// </summary>
	/// <param name="newZen">Zen data asset to transition to. </param>
	/// <param name="time">Duration of transition. </param>
	/// <param name="source">Origin of the zen change. </param>
	/// <returns>None</returns>
	IEnumerator ZenTransition(ZenType newZen, float time, IZenChanger source = null)
	{
		transitionActive = true;

		if (activeChanger != null)
			activeChanger.Deactivate();

		float elapsedTime = 0f;
		ZenType startZen = currentZen;
		ZenType temp = ScriptableObject.CreateInstance<ZenType>();

		// transitions to the new Zen state. 
		while (elapsedTime <= 1.0f)
		{
			temp.lightIntensity = Mathf.Lerp(startZen.lightIntensity, newZen.lightIntensity, Tween.EaseValue(Tween.EaseType.Linear, elapsedTime));
			temp.lightColor = Color.Lerp(startZen.lightColor, newZen.lightColor, Tween.EaseValue(Tween.EaseType.Linear, elapsedTime));
			temp.backgroundColor = Color.Lerp(startZen.backgroundColor, newZen.backgroundColor, Tween.EaseValue(Tween.EaseType.Linear, elapsedTime));
			currentZen = temp;
			elapsedTime += Time.deltaTime / time;
			yield return null;
		}
		_currentZen = newZen;
		transitionActive = false;

		if( source != null)
			activeChanger = source; 

		if (queuedZen != null)
		{
			StartCoroutine(ZenTransition(queuedZen, transitionTime));
		}
	}

	#region IZenChanger interface
	public void Deactivate()
	{
		ResetAll(); 
		
	}

	public void ResetAll()
	{
		// if already in default, ignore. 
		if (currentZen == defaultZen)
			return;

		// if a transition is active, add default zen to the queue. 
		if (transitionActive)
		{
			queuedZen = defaultZen;
			return;
		}

		// start transition to default. 
		StartCoroutine(ZenTransition(defaultZen, transitionTime));
	}
	#endregion

}
