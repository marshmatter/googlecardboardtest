﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class MakeZenType
{
	[MenuItem("Assets/Create/ZenType")]
	public static void CreateMyAsset()
	{
		ZenType asset = ScriptableObject.CreateInstance<ZenType>();

		AssetDatabase.CreateAsset(asset, "Assets/Data/NewZenType.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = asset;
	}
}
