﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Data", menuName = "Zen Type", order = 1)]
public class ZenType : ScriptableObject
{
	public Color lightColor = Color.blue;
	public float lightIntensity = 1.0f;
	public Color backgroundColor = Color.red;
}
