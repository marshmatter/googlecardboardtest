﻿using UnityEngine;
using System.Collections;

public class StateMonoBehaviour : MonoBehaviour
{
	public delegate IEnumerator StateMethod();
	public StateMethod state { get; private set; }
	public StateMethod lastState { get; private set; }
	public string stateName { get; private set; }
	public string lastStateName { get; private set; }

	public void SetState(StateMethod stateMethod)
	{
		if (stateMethod != state)
		{
			//if (gameObject.GetComponent<Player>() != null)
			//	Debug.LogWarning("setting state: " + stateMethod.Method.Name);

			StopAllCoroutines();

			lastStateName = stateName;
			stateName = stateMethod.Method.Name;

			//Debug.LogWarning("SetState: " + stateName);

			lastState = state;
			state = stateMethod;

			StartCoroutine(DoSetState());
		}
	}

	void OnEnable()
	{
		//if (state != null)
		//	SetState(state);
	}

	void OnDisable()
	{
		StopAllCoroutines();
		state = null;
	}

	IEnumerator DoSetState()
	{
		yield return null;
		StartCoroutine(state());
	}

	public void InvokeState(StateMethod stateMethod, float time)
	{
		StartCoroutine(DoInvokeState(stateMethod, time));
	}

	IEnumerator DoInvokeState(StateMethod stateMethod, float time)
	{
		yield return new WaitForSeconds(time);
		SetState(stateMethod);
	}
}