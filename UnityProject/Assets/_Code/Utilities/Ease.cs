﻿using UnityEngine;

static public class Ease
{
    static public float QuadIn(float t) { return t * t; }
    static public float QuadOut(float t) { return 1 - QuadIn(1 - t); }
    static public float QuadInOut(float t) { return (t <= 0.5f) ? QuadIn(t * 2) / 2 : QuadOut(t * 2 - 1) / 2 + 0.5f; }
    static public float CubeIn(float t) { return t * t * t; }
    static public float CubeOut(float t) { return 1 - CubeIn(1 - t); }
    static public float CubeInOut(float t) { return (t <= 0.5f) ? CubeIn(t * 2) / 2 : CubeOut(t * 2 - 1) / 2 + 0.5f; }
    static public float BackIn(float t) { return t * t * (2.70158f * t - 1.70158f); }
    static public float BackOut(float t) { return 1 - BackIn(1 - t); }
    static public float BackInOut(float t) { return (t <= 0.5f) ? BackIn(t * 2) / 2 : BackOut(t * 2 - 1) / 2 + 0.5f; }
    static public float ExpoIn(float t) { return Mathf.Pow(2, 10 * (t - 1)); }
    static public float ExpoOut(float t) { return 1 - ExpoIn(t); }
    static public float ExpoInOut(float t) { return t < .5f ? ExpoIn(t * 2) / 2 : ExpoOut(t * 2) / 2; }
    static public float SineIn(float t) { return -Mathf.Cos(Mathf.PI / 2 * t) + 1; }
    static public float SineOut(float t) { return Mathf.Sin(Mathf.PI / 2 * t); }
    static public float SineInOut(float t) { return -Mathf.Cos(Mathf.PI * t) / 2f + .5f; }
}