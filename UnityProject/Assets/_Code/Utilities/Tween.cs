﻿using UnityEngine;
using System.Collections;

public class TweenBase : MonoBehaviour
{
}

public class TweenMove : TweenBase
{
    public Vector3 start;
    public Vector3 end;
    public float time;
    public Tween.EaseType easeType;
    public bool isLocal;

    private float timer;

    void Update()
    {
        timer += Time.deltaTime;
        if (timer > time)
            timer = time;
        if (isLocal)
            transform.localPosition = start + (end - start) * Tween.EaseValue(easeType, timer / time);
        else
            transform.position = start + (end - start) * Tween.EaseValue(easeType, timer / time);
        if (timer >= time)
            Destroy(this);
    }
}

public class TweenScale : TweenBase
{
    public Vector3 start;
    public Vector3 end;
    public float time;
    public Tween.EaseType easeType;

    private float timer;

    void Update()
    {
        timer += Time.deltaTime;
        if (timer > time)
            timer = time;
        transform.localScale = start + (end - start) * Tween.EaseValue(easeType, timer / time);
        if (timer >= time)
            Destroy(this);
    }
}

public class TweenRotate : TweenBase
{
    public Quaternion start;
    public Quaternion end;
    public float time;
    public Tween.EaseType easeType;
    public bool isLocal;

    private float timer;

    void Update()
    {
        timer += Time.deltaTime;
        if (timer > time)
            timer = time;
        if (isLocal)
            transform.localRotation = Quaternion.Slerp(start, end, Tween.EaseValue(easeType, timer / time));
        else
            transform.rotation = Quaternion.Slerp(start, end, Tween.EaseValue(easeType, timer / time));
        if (timer >= time)
            Destroy(this);
    }
}

public class TweenSpin : TweenBase
{
    public float maxAngularSpeed;
    public float angularSpeed;
    public float acceleration;
    public float deceleration;
    public float time;
    public Vector3 axis;

    private int state;

    void Update()
    {
        if (state == 0)
        {
            angularSpeed += Time.deltaTime * acceleration;
            if (angularSpeed > maxAngularSpeed)
            {
                angularSpeed = maxAngularSpeed;
                state = 1;
            }
        }
        else if (state == 1)
        {
            time -= Time.deltaTime;
            if (time < 0f)
                state = 2;
        }
        else if (state == 2)
        {
            angularSpeed -= Time.deltaTime * deceleration;
            if (angularSpeed < 0f)
                Destroy(this);
        }
        gameObject.transform.localEulerAngles += angularSpeed * axis * Time.deltaTime;
    }
}

public class Tween : MonoBehaviour
{
    public enum EaseType
    {
        Linear,
        BackIn,
        BackOut,
        BackInOut,
        SineIn,
        SineOut,
        SineInOut,
        CubeIn,
        CubeOut,
        CubeInOut,
        ElasticIn,
        ElasticOut,
        ElasticInOut,
        Max
    }

    public static TweenScale ScaleFrom(GameObject gameObject, Vector3 scale, float time, EaseType easeType)
    {
        TweenScale tweenScale = gameObject.AddComponent<TweenScale>();
        tweenScale.start = scale;
        tweenScale.end = gameObject.transform.localScale;
        tweenScale.time = time;
        tweenScale.easeType = easeType;
        return tweenScale;
    }

    public static TweenScale ScaleTo(GameObject gameObject, Vector3 scale, float time, EaseType easeType)
    {
        TweenScale tweenScale = (TweenScale)gameObject.AddComponent<TweenScale>();
        tweenScale.start = gameObject.transform.localScale;
        tweenScale.end = scale;
        tweenScale.time = time;
        tweenScale.easeType = easeType;
        return tweenScale;
    }

    public static TweenMove MoveFrom(GameObject gameObject, Vector3 position, float time, EaseType easeType, bool isLocal)
    {
        TweenMove tweenMove = gameObject.AddComponent<TweenMove>();
        tweenMove.start = position;
        tweenMove.end = isLocal ? gameObject.transform.localPosition : gameObject.transform.position;
        tweenMove.time = time;
        tweenMove.easeType = easeType;
        tweenMove.isLocal = isLocal;
        return tweenMove;
    }

    public static TweenMove MoveTo(GameObject gameObject, Vector3 position, float time, EaseType easeType, bool isLocal)
    {
        TweenMove tweenMove = gameObject.AddComponent<TweenMove>();
        tweenMove.start = isLocal ? gameObject.transform.localPosition : gameObject.transform.position;
        tweenMove.end = position;
        tweenMove.time = time;
        tweenMove.easeType = easeType;
        tweenMove.isLocal = isLocal;
        return tweenMove;
    }

    public static TweenRotate RotateFrom(GameObject gameObject, Vector3 eulerAngles, float time, EaseType easeType, bool isLocal)
    {
        TweenRotate tweenRotate = gameObject.AddComponent<TweenRotate>();
        tweenRotate.start = Quaternion.Euler(eulerAngles);
        tweenRotate.end = isLocal ? gameObject.transform.localRotation : gameObject.transform.rotation;
        tweenRotate.time = time;
        tweenRotate.easeType = easeType;
        tweenRotate.isLocal = isLocal;
        return tweenRotate;
    }

    public static TweenRotate RotateTo(GameObject gameObject, Vector3 eulerAngles, float time, EaseType easeType, bool isLocal)
    {
        TweenRotate tweenRotate = gameObject.AddComponent<TweenRotate>();
        tweenRotate.start = isLocal ? gameObject.transform.localRotation : gameObject.transform.rotation;
        tweenRotate.end = Quaternion.Euler(eulerAngles);
        tweenRotate.time = time;
        tweenRotate.easeType = easeType;
        tweenRotate.isLocal = isLocal;
        return tweenRotate;
    }

    public static TweenSpin Spin(GameObject gameObject, float maxAngularSpeed, float accelerationTime, float decelerationTime, float time, Vector3 axis)
    {
        TweenSpin tweenSpin = gameObject.AddComponent<TweenSpin>();
        tweenSpin.maxAngularSpeed = maxAngularSpeed;
        tweenSpin.acceleration = maxAngularSpeed / accelerationTime;
        tweenSpin.deceleration = maxAngularSpeed / decelerationTime;
        tweenSpin.time = time;
        tweenSpin.axis = axis;
        return tweenSpin;
    }

    public static void Stop(GameObject gameObject)
    {
        foreach (TweenBase tweenBase in gameObject.GetComponents<TweenBase>())
        {
            Destroy(tweenBase);
        }
    }

    public static int Count(GameObject gameObject)
    {
        return gameObject.GetComponents<TweenBase>().Length;
    }

    public static float EaseValue(EaseType easeType, float t)
    {
        switch (easeType)
        {
            case EaseType.BackIn:
                return Ease.BackIn(t);
            case EaseType.BackOut:
                return Ease.BackOut(t);
            case EaseType.BackInOut:
                return Ease.BackInOut(t);
            case EaseType.SineIn:
                return Ease.SineIn(t);
            case EaseType.SineOut:
                return Ease.SineOut(t);
            case EaseType.SineInOut:
                return Ease.SineInOut(t);
            case EaseType.CubeIn:
                return Ease.CubeIn(t);
            case EaseType.CubeOut:
                return Ease.CubeOut(t);
            case EaseType.CubeInOut:
                return Ease.CubeInOut(t);
        }
        return t;
    }
}